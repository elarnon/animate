let buffer_of_fdescr fd chunk_size =
  let chunk = Bytes.create chunk_size in
  let buffer = Buffer.create chunk_size in
  let rec loop () =
    try
      let read = Unix.read fd chunk 0 chunk_size in
      Buffer.add_bytes buffer (Bytes.sub chunk 0 read);
      if read = chunk_size then loop () else ()
    with
    | Unix.Unix_error (Unix.EAGAIN, _, _)
    | Unix.Unix_error (Unix.EWOULDBLOCK, _, _) ->
      () in
  let get () =
    Buffer.contents buffer
  in loop, get


let lem_pm_root =
  Filename.concat Config.animate_root "lem_patterns"
let lem_z3_root =
  Filename.concat Config.animate_root "lem_z3"
let lib_dir =
  Filename.concat Config.animate_root "libs"
let z3_build = Filename.concat Config.z3_root "build"

let log = print_endline

let debug = ref false

(* Temporary files handling *)
module Temporaries : sig
  val fresh : string -> string -> string
  val garbage : string -> unit
end = struct

  let temp_dir =
    (* TODO: Repeat if it fails. *)
    let tmp_file = Filename.temp_file "animate_" "" in
    Unix.unlink tmp_file;
    Unix.mkdir tmp_file 0o700;
    tmp_file

  let garbage_ = ref []

  let unix_cleanup fn =
    let open Unix in
    fun x ->
      try fn x with
      | Unix_error (err, fn, "") ->
        Format.eprintf "WARNING: \"%s\" failed: %s@." fn (error_message err)
      | Unix_error (err, fn, arg) ->
        Format.eprintf "WARNING: \"%s\" failed on \"%s\": %s@." fn arg (error_message err)

  let cleanup () =
    (* List.iter (unix_cleanup Unix.unlink) !garbage; *)
    (* unix_cleanup Unix.rmdir temp_dir; *)
    garbage_ := []

  let () =
    at_exit cleanup

  let fresh (prefix : string) (suffix : string) : string =
    let fname =
      Filename.temp_file ~temp_dir:temp_dir prefix suffix in
    garbage_ := fname :: !garbage_;
    fname

  let garbage (fname : string) =
    garbage_ := fname :: !garbage_
end

(* Calling external programs with wrappers for sanity *)
let call ?(env:string array option) (prog : string) (args : string list)
  : int * bytes * bytes =
  if !debug then
    Format.eprintf "*DEBUG* Calling %s %s@." prog (String.concat " " args);
  let read_in, write_in = Unix.pipe () in
  let read_out, write_out = Unix.pipe () in
  let read_err, write_err = Unix.pipe () in
  Unix.set_nonblock read_out; Unix.set_nonblock read_err;
  let argv = Array.of_list @@ prog :: args in
  let fn =
    match env with
    | None -> Unix.create_process prog argv
    | Some env' -> Unix.create_process_env prog argv env' 
  in
  Unix.close write_in;
  let pid = fn read_in write_out write_err in
  (* These streams are not needed anymore *)
  Unix.close read_in; Unix.close write_out; Unix.close write_err;
  let chunk_size = 1024 in
  let out_loop, out_get = buffer_of_fdescr read_out chunk_size in
  let err_loop, err_get = buffer_of_fdescr read_err chunk_size in
  let rec loop () =
    let cpid, status = Unix.waitpid [Unix.WUNTRACED; Unix.WNOHANG] pid in
    if cpid = 0 then begin
      out_loop ();
      err_loop ();
      loop ()
    end else status
  in let status = loop () in
  out_loop (); err_loop (); Unix.close read_out; Unix.close read_err;
  match status with
  | Unix.WEXITED retcode ->
    retcode, out_get (), err_get ()
  | _ ->
    failwith "External program killed" (* TODO *)

let call_exn ?env (prog : string) (args : string list) : unit =
  let ret, _, err = call ?env prog args (* TODO *) in
  if ret != 0 then begin
    Format.eprintf "Error while calling: `%s %s`@." prog (String.concat " " args);
    Format.eprintf "Output was: @.";
    prerr_endline err; exit (-1)
  end

(*****************************************************************************)
(* Primitives to call Ott                                                    *)
(*****************************************************************************)

module Ott : sig
  type t
  val create : string -> string -> t
  val build_langs : t -> string list -> (string * string) list
  val filter : t -> [ `Lem ] -> string -> string
end = struct
  type t =
    { path : string
    ; src : string
    }

  let create path src =
    { path; src }

  let build_langs ott langs =
    let base = Filename.chop_extension @@
      Filename.basename ott.src in
    let mapping = List.rev_map (fun lang ->
        (lang, Temporaries.fresh base ("." ^ lang)))
        langs in
    let args =
      ott.src ::
      List.fold_left (fun args (_, file) ->
          "-o" :: file :: args) [] mapping in
    call_exn ott.path args;
    mapping

  let filter ott lang infile =
    let filter_opt =
      match lang with
      | `Lem -> "-lem_filter"
    in
    let filtered = Temporaries.fresh "filtered_" ".lem" in
    call_exn ott.path [ott.src; filter_opt; infile; filtered];
    filtered
end

(*****************************************************************************)
(* Parsing of execution files                                                *)
(*****************************************************************************)

module Parser = struct
  type t =
    { source : string
    ; pos : int
    }

  type 'a monad =
    t -> (t * 'a) option

  let return (x : 'a) : 'a monad =
    function parser ->
      Some (parser, x)

  let (>>=) (parsing : 'a monad) (fn : 'a -> 'b monad) : 'b monad =
    fun parser ->
      match parsing parser with
      | None -> None
      | Some (parser, out) -> fn out parser

  let run (parsing : 'a monad) (str : string) : 'a option =
    match parsing { source = str; pos = 0 } with
    | None -> None
    | Some (_, out) -> Some out

  let (>>) (parsing : 'a monad) (next : 'b monad) : 'b monad =
    parsing >>= fun _ -> next

  let maybe (parsing : 'a monad) : 'a option monad =
    fun parser ->
      match parsing parser with
      | None -> Some (parser, None)
      | Some (parser, out) -> Some (parser, Some out)

  let rec any (args : 'a monad list) : 'a monad =
    match args with
    | [] -> fun _ -> None
    | left :: right ->
      function parser ->
        match left parser with
        | None -> any right parser
        | Some (parser, out) -> Some (parser, out)

  let rec eat_space ?(required=false) ({ source; pos } as parser) =
    if pos >= String.length source then None else
    if source.[pos] = ' ' then
      eat_space ~required:false { parser with pos = pos + 1 }
    else if required then
      None
    else
      Some (parser, ())

  let rec several (sub : 'a monad) : 'a list monad =
    maybe sub >>= function
    | None -> return []
    | Some x -> several sub >>= fun xs -> return @@ x :: xs

  let word ({ source; pos } as parser) =
    let len = String.length source in
    let rec find_space pos =
      if pos >= len then
        len - 1
      else if source.[pos] = ' ' then
        pos
      else
        find_space (pos + 1) in
    let epos = find_space pos in
    if epos = pos then None else
      Some ({ parser with pos = epos },
            String.sub source pos (epos - pos))

  let number ({ source; pos } as parser) =
    let len = String.length source in
    let is_num chr =
      let ccode = Char.code chr in
      ccode >= Char.code '0' && ccode <= Char.code '9' in
    let marker = ref pos in
    while is_num (source.[!marker]) && !marker < len do
      marker := !marker + 1
    done;
    if !marker = pos then None else
      Some ({ parser with pos = !marker },
            int_of_string @@ String.sub source pos (!marker - pos))

  let exact str ({ source; pos} as parser) =
    if str = String.sub source pos
         (min (String.length source - pos) (String.length str)) then
      Some ({ parser with pos = pos + String.length str }, str)
    else None

  let eol ({ source; pos } as parser) =
    if String.length source = pos then
      Some (parser, ())
    else
      None

end

open Parser

let as_clause =
  exact "as" >> eat_space >> word

let rec ios parser =
  (any [exact "i"; exact "o"] >>= fun io ->
   eat_space >> exact "->" >>= fun _ ->
   eat_space >> maybe ios >>= function
   | None -> return [io]
   | Some ios -> return @@ io :: ios) parser

let rec typs parser =
  (word >>= fun t ->
   eat_space >> exact "->" >>= fun _ ->
   eat_space >> maybe typs >>= function
   | None -> return [t]
   | Some ts -> return @@ t :: ts) parser

let modelist =
  ios >>= fun mode ->
  eat_space >> word >>= fun out ->
  return (mode, out)

type exe_line =
  { relname : string
  ; mode : string list
  ; monad : string
  ; alias : string
  ; mangled : string
  }

type smt_line =
  { relname : string
  ; alias : string
  ; nargs : int
  }

let mangle rel modes monad =
  rel ^ "_" ^ String.concat "_" modes ^ "_" ^ monad

let exe_line =
  word >>= fun relname ->
  eat_space >> modelist >>= fun (mode, monad) ->
  eat_space >> maybe as_clause >>= fun as_clause ->
  let alias = match as_clause with
    | Some name -> name
    | None -> relname in
  return { relname; mode; monad; alias;
           mangled = mangle relname mode monad }

let concrete_line =
  word >>= fun vname ->
  eat_space >> number >>= fun vnum ->
  return (vname, vnum)

type special_type =
  | Exe of exe_line
  | Concrete of string * int
  | Exists of string * string
  | Smt of smt_line

let special_line =
  exact "{{" >> eat_space >>
  any [ exact "exe"; exact "smt"; exact "concrete"; exact "exists" ] >>= (function
      | "concrete" ->
        eat_space >> concrete_line >>= fun (n, v) -> return (Concrete (n, v))
      | "exists" ->
        eat_space >> word >>= fun ename ->
        eat_space >> exact ":" >> eat_space >>
        word >>= fun etype -> return (Exists (ename, etype))
      | "smt" ->
        eat_space >> word >>= fun relname ->
        eat_space >> several (eat_space >> exact "_") >>= fun lst ->
        eat_space >> maybe as_clause >>= fun alias ->
        let alias =
          match alias with
          | None -> relname
          | Some a -> a in
        return (Smt { relname; alias; nargs = List.length lst })
      | _ ->
        eat_space >> exe_line >>= fun x -> return (Exe x)
    ) >>= fun out ->
  eat_space >> exact "}}" >> eol >>= fun () ->
  return out

let parse_line = run special_line

let inout_args mode args =
  let in_args, out_args =
    List.partition (fun (m, _) -> m = "i")
      (List.combine mode args) in
  List.map snd in_args, List.map snd out_args

type relname = string

type exename = string

type varname = string

type typename = string

type pm_info =
  { pm_aliases : (exename, exe_line) Hashtbl.t
  (** Mapping from a pattern-matching alias to its execution mode *)
  ; pm_modes : (relname, exe_line list) Hashtbl.t
  (** Mapping from a relation's name to its various execution modes *)
  ; pm_queries : (exename * (string list * string list)) list
  (** Ordered list of queries, which may contain dependencies.
      A query consists of a tuple (exename, (in_args, out_args)). *)
  }

type smt_info =
  { smt_aliases : (exename, smt_line) Hashtbl.t
  (** Mapping from a SMT alias to the associated relation *)
  ; smt_queries : (exename * string list) list
  (** Ordered list of queries, which may contain dependencies.
      A query consists of a pair (exename, args). *)
  ; exists : (varname, typename) Hashtbl.t
  (** List of existentially quantified SMT variables *)
  }

type exe_file =
  { pm : pm_info
  ; smt : smt_info
  ; concrete : (varname, int) Hashtbl.t
  (** Mapping from variable names to a concrete representation for execution *)
  }

let parse_exe_file ic =
  let pm_aliases = Hashtbl.create 17 in
  let pm_modes = Hashtbl.create 17 in
  let smt_aliases = Hashtbl.create 17 in
  let smt_exists = Hashtbl.create 17 in
  let concrete = Hashtbl.create 17 in
  let mode = ref `Exe in
  let pm_queries = ref [] in
  let smt_queries = ref [] in
  begin try
      while true do
        let line = input_line ic in
        match !mode with
        | `Exe ->
          begin match parse_line line with
            | Some (Exe exe) ->
              if Hashtbl.mem pm_aliases exe.alias then begin
                Format.eprintf "Warning: alias `%s' defined twice@." exe.alias;
                Format.eprintf "Only the last definition will be used@.";
              end;
              Hashtbl.replace pm_aliases exe.alias exe
            | Some (Concrete (n, v)) ->
              if Hashtbl.mem concrete n then begin
                Format.eprintf "Warning: name `%s' mapped to multiple concrete values@." n;
                Format.eprintf "Only the last concrete value will be used.@.";
              end;
              Hashtbl.replace concrete n v
            | Some (Exists (v, t)) ->
              if Hashtbl.mem smt_exists v then begin
                Format.eprintf "Warning: name `%s' defined several times as an existential variable@." v;
              end;
              Hashtbl.replace smt_exists v t
            | Some (Smt s) ->
              if Hashtbl.mem smt_aliases s.alias then begin
                Format.eprintf "Warning: alias `%s' defined twice@." s.alias;
                Format.eprintf "Only the last definition will be used@.";
              end;
              Hashtbl.replace smt_aliases s.alias s
            | None ->
              if String.length line > 0 then
                failwith @@ "Expected empty line, got `" ^ line ^ "` instead.";
              mode := `Newrel
          end
        | `Newrel ->
          begin try
              let { mode = relmode; _ } = Hashtbl.find pm_aliases line in
              mode := `Rel (line, List.length relmode, [])
            with Not_found ->
              try
                let { nargs; _ } = Hashtbl.find smt_aliases line in
                mode := `Smt (line, nargs, [])
              with Not_found ->
                failwith @@ "Unknown relation " ^ line
          end
        | `Rel (relname, 0, args) ->
          if String.length line > 0 then
            failwith @@
            "Relation `" ^ relname ^ "' has no remaining arguments, expected an empty line."
          else begin
            mode := `Newrel;
            let { mode = relmode; _ } = Hashtbl.find pm_aliases relname in
            pm_queries := (relname, inout_args relmode @@ List.rev args) :: !pm_queries;
          end
        | `Rel (relname, remaining, args) ->
          mode := `Rel (relname, remaining - 1, line :: args)
        | `Smt (relname, 0, args) ->
          if String.length line > 0 then
            failwith @@
            "Relation `" ^ relname ^ "' has no remaining arguments, expected an empty line."
          else begin
            mode := `Newrel;
            smt_queries := (relname, List.rev args) :: !smt_queries;
          end
        | `Smt (relname, remaining, args) ->
          mode := `Smt (relname, remaining - 1, line :: args)
      done
    with End_of_file ->
      match !mode with
      | `Rel (relname, 0, args) ->
        mode := `Newrel;
        let { mode = relmode; _ } = Hashtbl.find pm_aliases relname in
        pm_queries := (relname, inout_args relmode @@ List.rev args) :: !pm_queries;
      | `Smt (relname, 0, args) ->
        mode := `Newrel;
        smt_queries := (relname, List.rev args) :: !smt_queries
      | `Rel (_, remaining, _) | `Smt (_, remaining, _) ->
        failwith @@ Format.sprintf "Nonterminated query, %d missing arguments." remaining
      | `Exe ->
        failwith "No queries found."
      | `Newrel -> ()
  end;
  let modes_by_name = Hashtbl.create 17 in
  Hashtbl.iter (fun _ (exe : exe_line) ->
      try
        let old = Hashtbl.find modes_by_name exe.relname in
        Hashtbl.replace modes_by_name exe.relname (exe::old)
      with Not_found ->
        Hashtbl.add modes_by_name exe.relname [exe]) pm_aliases;
  { pm =
       { pm_aliases
       ; pm_modes = modes_by_name
       ; pm_queries = List.rev !pm_queries
       }
  ; smt =
      { smt_aliases
      ; smt_queries = List.rev !smt_queries
      ; exists = smt_exists
      }
  ; concrete
  }

let parse_args () = 
  let do_z3 = ref true in
  let do_pm = ref true in
  let args = ref [] in

  let anon_arg name =
    args := name :: !args in

  let args_spec = Arg.align [
      ("-no-z3", Arg.Clear do_z3, " Disable the Z3 backend");
      ("-no-pm", Arg.Clear do_pm, " Disable the pattern-matching backend");
      ("-debug", Arg.Set debug, " Enable various debug behaviors TODO");
      ("--", Arg.Rest anon_arg, " Treat remaining options as file names");
    ] in

  let usage =
    "usage: animate [-debug] file.ott file.exe" in

  Arg.parse args_spec anon_arg usage;

  match List.rev !args with
  | [ ott; exe ] -> (ott, exe, (!do_z3, !do_pm))
  | _ ->
    Format.eprintf "error: expected exactly two arguments@.";
    Arg.usage args_spec usage;
    exit 1

module Lem = struct
  type t =
    { path : string
    ; ocamlopt : string
    ; library_dir : string
    }

  let create root ocamlopt =
    { path = Filename.concat root "lem"
    ; library_dir =
        Filename.concat
          (Filename.concat root "ocaml-lib")
          "_build"
    ; ocamlopt
    }

  let build_libs lem libdir libs =
    let all_ok =
      List.for_all (fun lib ->
          Sys.file_exists
            (Filename.concat libdir @@ lib ^ ".cmxa"))
        libs in
    if not all_ok then
      let lem_files =
        List.map (fun lib ->
            Filename.concat libdir @@ lib ^ ".lem")
          libs in
      call_exn lem.path ("-ocaml" :: lem_files);
      List.iter (fun lib ->
          call_exn lem.ocamlopt
            (["-I"; lem.library_dir;
              "-I"; libdir;
              "-a"; "-o"; Filename.concat libdir @@ lib ^ ".cmxa";
              Filename.concat libdir @@ lib ^ ".ml"]))
        libs

  let translate_file lem fname =
    call_exn lem.path (["-ocaml"; fname]);
    let ml = Filename.chop_extension fname ^ ".ml" in
    Temporaries.garbage ml; ml

  let build_file lem libdir libs fname =
    call_exn lem.path (["-lib"; libdir; "-ocaml"; fname]);
    let raw = Filename.chop_extension fname in
    let ml = raw ^ ".ml" in
    let out = raw ^ ".native" in
    build_libs lem libdir libs;
    call_exn lem.ocamlopt
      (["-I"; lem.library_dir;
        "-I"; libdir;
        "-o"; out] @
       (List.map (fun lib -> lib ^ ".cmxa") libs) @
       [ml]);
    out

  let build_lem_queries (exefile : exe_file) arg_types ppf =
    (*  let inputs =
        List.fold_left (fun inputs (_, (inp, _)) ->
            List.rev_append inp inputs) [] queries in *)
    let possible_types = Hashtbl.create 17 in
    Hashtbl.iter (fun _ tys ->
        List.iter (fun ty ->
            Hashtbl.replace possible_types ty ()) tys) arg_types;
    Format.fprintf ppf "open import Exec@.@.";
    Format.fprintf ppf "let result = @.";
    Hashtbl.iter (fun n v ->
        Format.fprintf ppf "let %s = %d in@." n v) exefile.concrete;
    List.iter (fun (alias, (inps, outs)) ->
        let { monad; mangled; mode; relname; _ } = Hashtbl.find exefile.pm.pm_aliases alias in
        let out_typs =
          List.flatten @@
          List.map2 (fun m ty ->
              if m = "o" then [ty] else [])
            mode (Hashtbl.find arg_types relname) in
        Format.fprintf ppf "Exec.bind (Exec.of_%s (%s " monad mangled;
        List.iter (fun inp -> Format.fprintf ppf "([[ %s ]])" inp) inps;
        Format.fprintf ppf ")) (fun (";
        List.iteri (fun i out ->
            if i != 0 then Format.fprintf ppf ",";
            Format.fprintf ppf " %s " out) outs;
        Format.fprintf ppf ") -> @.";
        List.iter2 (fun out ty ->
            Format.fprintf ppf
              "let () = Exec.print \"%s\" (pp_%s %s) in@." out ty out)
          outs out_typs)
      exefile.pm.pm_queries;
    Format.fprintf ppf
      "let () = Exec.display \"All queries satisfied.\" in@.";
    Format.fprintf ppf "Exec.return ()@.";
    List.iter (fun _ -> Format.fprintf ppf ")@.") exefile.pm.pm_queries;
    Format.fprintf ppf "let () = let nb = Exec.run_all result in@.";
    Format.fprintf ppf "if nb = 0 then Exec.display \"No solution found.\" else ()@."


  let indreln_line =
    maybe (eat_space >> exact "and" >> eat_space) >>
    exact "[" >> eat_space >> word >>= fun relname ->
    eat_space >> exact ":" >> eat_space >>
    typs >>= fun typs ->
    return (relname, typs)

  let list_elem ~fixup str =
    if fixup then
      let len = String.length str in
      let pat = "List.elem" in
      let pat_len = String.length pat in
      let res = ref [] in
      let start = ref 0 in
      for i = 0 to len - pat_len - 1 do
        if String.sub str i pat_len = pat then begin
          res := String.sub str !start (i - !start) :: !res;
          res := "list_elem" :: !res;
          start := i + pat_len
        end
      done;
      res := String.sub str !start (len - !start) :: !res;
      Bytes.concat "" @@ List.rev !res
    else
      str

  let copy_get_args ?(fixup=false) lemfile outfile =
    let arg_types_tbl = Hashtbl.create 17 in
    let module X = struct
      exception E
    end in 
    try
      while true do
        let line = input_line lemfile in
        let len = String.length line in
        try
          if len > 0 then begin (* && line.[0] == '[' && line.[len - 1] == ']' then begin *)
            let prefix = String.sub line 0 (len - 1) in
            let relname, arg_types =
              match run indreln_line prefix with
              | None -> raise X.E
              | Some (name, typs) -> name, typs in
            Hashtbl.replace arg_types_tbl relname arg_types;
          end;
          output_string outfile (list_elem ~fixup line ^ "\n")
        with
        | X.E -> output_string outfile (list_elem ~fixup line ^ "\n")
        | Not_found ->
          output_string outfile (list_elem ~fixup line ^ "\n")
          (* TODO: Print missing relation *)
      done;
      assert false
    with End_of_file -> arg_types_tbl (* TODO check *)

  let transform_lem lemfile exefile outfile =
    let arg_types_tbl = Hashtbl.create 17 in
    let module X = struct
      exception E
    end in 
    try
      while true do
        let line = input_line lemfile in
        let len = String.length line in
        try
          if len > 0 then(* && line.[0] == '[' && line.[len - 1] == ']' then *)
            let prefix = String.sub line 0 (len - 1) in
            let relname, arg_types =
              match run indreln_line prefix with
              | None -> raise X.E
              | Some (name, typs) -> name, typs
            in
            Hashtbl.replace arg_types_tbl relname arg_types;
            let modinfos = Hashtbl.find exefile.pm.pm_modes relname in
            let append =
              List.rev_map (fun exe ->
                  let mode =
                    List.map (function
                        | "i" -> "input"
                        | _ -> "output") exe.mode
                  in
                  exe.mangled ^ ": " ^
                  String.concat " -> " mode ^
                  " -> " ^ exe.monad) modinfos in
            output_string outfile
              (prefix ^ ";" ^ String.concat "; " append ^ "]\n")
          else
            output_string outfile (line ^ "\n")
        with
        | X.E ->
          output_string outfile (line ^ "\n")
        | Not_found ->
          output_string outfile (line ^ "\n")
          (* TODO: Print missing relation *)
      done;
      assert false
    with End_of_file ->
      Hashtbl.iter (fun relname _ ->
          if not @@ Hashtbl.mem exefile.pm.pm_modes relname then
            Format.eprintf "Warning: no mode defined for relation %s@." relname)
        arg_types_tbl;
      arg_types_tbl

  let apply_exe lem lemfile exe_info =
    (* Create a temporary Lem file to store the Lem code transformed to include
       execution information in the indrelns as well as the code for Lem queries *)
    let with_exes = Temporaries.fresh "temp_" ".lem" in
    let ic = open_in lemfile in
    let oc = open_out with_exes in
    let arg_types =
      transform_lem ic exe_info oc in
    close_in ic;

    (* Add the queries to the temporary Lem file *)
    build_lem_queries exe_info arg_types
      (Format.formatter_of_out_channel oc);
    close_out oc;
    with_exes

  let add_printing_z3 (smt_info : smt_info) arg_types ppf =
    Format.fprintf ppf "let print_binding binding = @.";
    Format.fprintf ppf "match binding with@.";
    Hashtbl.iter (fun v t ->
        Format.fprintf ppf
          "| (\"%s\", x) -> print_endline @@@@ \"%s = \" ^ pp_%s (Obj.obj x)@." v v t)
      smt_info.exists;
    Format.fprintf ppf "| (varname, _) -> Format.printf \"ERROR: Invalid name %%s@@.\" varname@.";
    Format.fprintf ppf "@.@.";
    Format.fprintf ppf "let rec print_bindings bindings =@.";
    Format.fprintf ppf "match bindings with@.";
    Format.fprintf ppf "| [] -> ()@.";
    Format.fprintf ppf "| binding :: bindings' -> let _ = print_binding binding in@.";
    Format.fprintf ppf "print_bindings bindings'@.@."

  let build_lem_z3_queries exe_info arg_types ppf =
    Hashtbl.iter (fun n v ->
        Format.fprintf ppf "let %s : int = %d@." n v)
      exe_info.concrete;
    Format.fprintf ppf "assert z3_query: ";
    let buf = Buffer.create 128 in
    let ppf_dup = Format.formatter_of_buffer buf in
    let all_outs =
      Hashtbl.fold (fun v t l -> v :: l) exe_info.smt.exists [] in
    begin match all_outs with
      | out :: outs' ->
        Format.fprintf ppf_dup "exists %s" out;
        List.iter (fun out ->
            Format.fprintf ppf_dup " %s" out)
          outs';
        Format.fprintf ppf_dup ". "
      | _ -> ()
    end;
    Format.fprintf ppf_dup "true@.";
    List.iter (fun (alias, args) ->
        Format.fprintf ppf_dup " && @.";
        let exeline = Hashtbl.find exe_info.smt.smt_aliases alias in
        Format.fprintf ppf_dup "%s" exeline.relname;
        List.iter (fun arg ->
            Format.fprintf ppf_dup " ([[ %s ]]) " arg)
          args;
        Format.fprintf ppf_dup "@.")
      exe_info.smt.smt_queries;
    let str = Buffer.to_bytes buf in
    Format.printf "Query will be: @.%s@." str;
    Format.fprintf ppf "%s@." str

  let build_exe_z3 ott lem lemfile exe_info =
    let with_exe = Temporaries.fresh "z3_" ".lem" in
    let ic = open_in lemfile in
    let oc = open_out with_exe in
    output_string oc @@
    ("open import Pervasives\n"
     ^ "let rec list_elem (x:int) ys =\n"
     ^ "match ys with\n"
     ^ "| [] -> false\n"
     ^ "| y :: ys' -> x = y || list_elem x ys\n"
     ^ "end\n\n");
    let arg_types = copy_get_args ~fixup:true ic oc in
    close_in ic;
    build_lem_z3_queries exe_info arg_types
      (Format.formatter_of_out_channel oc);
    close_out oc;
    let lem_out = Ott.filter ott `Lem with_exe in
    translate_file lem lem_out

  let build_printing_z3 lem lemfile exe_info module_name =
    let with_printing = Temporaries.fresh "printing_" ".lem" in
    let ic = open_in lemfile in
    let oc = open_out with_printing in
    let arg_types = copy_get_args ic oc in
    close_in ic;
    close_out oc;
    let ml_print = translate_file lem with_printing in
    let final_print = Temporaries.fresh "printing_final_" ".ml" in
    let ic = open_in ml_print in
    let oc = open_out final_print in
    try
      while true do
        let line = input_line ic in
        output_string oc (line ^ "\n")
      done;
      assert false
    with End_of_file ->
      close_in ic;
      add_printing_z3 exe_info arg_types
        (Format.formatter_of_out_channel oc);
      output_string oc @@
      ("let _ =\n"
       ^ "match " ^ module_name ^ ".output with\n"
       ^ "| (Z3.Solver.SATISFIABLE, tbl) -> print_endline \"Query is satisfiable with assignment:\"; print_bindings tbl\n"
       ^ "| _ -> print_endline \"Unable to find a solution.\"\n");
      close_out oc;
      final_print

  let apply_exe_z3 ott lem_raw lem_z3 lemfile exe_info =
    let with_exe = build_exe_z3 ott lem_z3 lemfile exe_info  in
    let module_name =
      String.capitalize @@
      Filename.chop_extension @@
      Filename.basename with_exe in
    let with_printing = build_printing_z3 lem_raw lemfile exe_info.smt module_name in
    with_exe, with_printing

  let compile_for_z3 lem z3_ml_file printing_ml_file =
    let z3_api = Filename.concat z3_build "api" in
    let z3_build_api_ml = Filename.concat z3_api "ml" in
    let temp_root = Filename.dirname z3_ml_file in
    let native_out = (Filename.chop_extension printing_ml_file) ^ ".native" in
    call_exn "ocamlopt"
      ["-cclib"; "-L"; "-cclib"; z3_build; "-cclib"; "-lz3"
      ; "-I"; temp_root; "-I"; z3_build_api_ml
      ; "-I"; lib_dir; "-I"; lem.library_dir
      ; "nums.cmxa"; "z3ml.cmxa"; "extract.cmxa"
      ; Filename.concat lib_dir "z3driver.ml"
      ; z3_ml_file; printing_ml_file; "-o"; native_out];
    native_out

  let z3_command =
    "ocamlopt -cclib -L -cclib z3/build -cclib -lz3 -I z3/build/api/ml -I lem_lib_dir nums.cmxa z3ml.cmxa z3driver.cmxa {file}.ml -o {file}.native"
  let z3_exe = "LD_LIBRARY_PATH=z3/build {file}.native"
end

let _ =
  let ott_file, exe_file, opts = parse_args () in
  let ott = Ott.create Config.ott_path ott_file in
  Format.printf "Parsing the exe file...@.";
  let exe_info =
    parse_exe_file (open_in exe_file) in
  Format.printf "Building the Lem file...@.";
  let langs = Ott.build_langs ott ["lem"] in
  let lemfile = List.assoc "lem" langs in
  let lem = Lem.create lem_pm_root "ocamlopt" in

  let do_z3, do_pm = opts in

  if do_pm then begin

    Format.printf "Adding the pattern-matching execution info to the Lem file...@.";
    let with_exes = Lem.apply_exe lem lemfile exe_info in
    Format.printf "Transforming the Ott terms into Lem syntax...@.";
    let filtered = Ott.filter ott `Lem with_exes in

    (* Build and call the final executable *)
    Format.printf "Building and compiling the pattern-matching ocaml file...@.";
    let native_path = Lem.build_file lem lib_dir ["exec"] filtered in
    Format.printf "Calling the pattern-matching backend...@.";
    let retcode, out, err = call native_path [] in
    print_endline out
  end;

  if do_z3 then begin

    let lem_z3 = Lem.create lem_z3_root "ocamlopt" in
    Format.printf "Adding the Z3 execution info to the Lem file...@.";
    let exe, print = Lem.apply_exe_z3 ott lem lem_z3 lemfile exe_info in
    Format.printf "Building and compiling the Z3 Ocaml file...@.";
    let z3_native = Lem.compile_for_z3 lem exe print in
    Format.printf "Calling the Z3 backend...@.";
    let retcode, out, err =
      call ~env:[| "LD_LIBRARY_PATH=" ^ z3_build
                 ; "DYLD_LIBRARY_PATH=" ^ z3_build
                |] z3_native [] in
    print_endline out

  end
