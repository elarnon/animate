# Installation

To get animate in a working state, you need to perform the following steps:

 - Install the experimental Ott version with pretty-printing
 - Install and build Z3 from source [https://z3.codeplex.com].
   You need to use the branch "unstable", and compile with the
	 OCaml API enabled:
	 
	   git clone https://git01.codeplex.com/z3 
		 cd z3
		 git checkout unstable
		 ./configure --ml
		 cd build
		 make

	 Animate is directly using Z3's build directory rather than installed
	 libraries at the moment.
 - Copy the config.ml.example file into config.ml and edit the variables
   in it according to your own settings.
 - Execute install.sh in Lem's top directory.
	 This will clone the Lem fork at
	 [https://bitbucket.org/elarnon/lem], and compile the two Lem versions - with
	 pattern-matching, and with compilation to Z3 - required by animate to
	 operate.
 - Execute `make` (or `ocamlbuild animate.native`) to build animate's own
	 executable.

# Usage

	animate.native lang.ott relations.exe [-no-z3] [-no-pm]

The language file lang.ott needs to have some restrictions for the moment.
In particular, it must not generate Lem code that use other standard library
functions than List.elem (due to a bug in the Z3 backend).

Moreover, extra care must be taken not to have name conflicts - due to the
implementation of the Z3 backend, it may rename variables that should be left
as is, and it animate will crash if these name conflict occur.  You should
program as if all identifiers (be it types, variables, or anything else) must
be unique in a case insensitive way (don't use a type [t] and a variable [T]).
This is true for both the variables defined by the language Ott file AND the
relation execution file.

# Syntax for relation execution files

A relation execution file (.exe) starts by one or more {{ exe }} blocks, whose syntax is:

{{ exe relname i -> o -> list }}

with as much i/o modes as there are arguments for the relation. The semantics
of the [i -> o -> list] part is the same as in an indreln block in Lem, and
[relname] is a relation name as defined in the Ott language file.
Additionally, {{ exe }} block can have an additional parameter:

{{ exe relname o -> i -> list as alias }}

This allows to define several modes for the same relation.

Then, {{ concrete }} blocks must follow, one for each free variable that will
occur in the subsequent queries.
The syntax is:

{{ concrete x 0 }}

where [x] is the name of the variable and [0] is a number that will be used
internally to make the computation, and display results to the end user.

After a line break, queries can be made. The shape of queries is as follow:

 - First on a line of its own, the name of the relation (or rather, an alias
	 name).

 - Then, on the following N subsequent lines (where N is the arity of the
	 relation), the relation's arguments.
	 For an input argument, those can be any expression in the language defined
	 by the Ott language file (e.g. a valid example could be [(\x. x) y]).
	 For an output argument, those must be a single identifier that will be bound
	 to an output of the relation for the subsequent queries, and can be reused.

See examples/st/test.exe for an example, and examples/st/st.ott for the
associated language file.

Note: you should either use a {{ lex alphanum }} annotation on your
metavariables, or use the prefixed names (e.g. x0, x1, x2, etc.) to get the
corresponding type in your queries.

# Semantics

The Z3 backend translates the query to unrolling SMT and will either returns
that there is no answer/that it can't conclude, loop infinitely, or return a
*single* satisfying assignment for all of the queries at once.
Note: the mode information do not matter to the Z3 backend.

The pattern-matching backend will generate in order the outputs for each query,
then perform the subsequent ones with the correct binding.
