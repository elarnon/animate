metavar termvar, x ::=
  {{ lem int }} {{ lex alphanum }}

indexvar index, n, m ::=
  {{ lem int }}

grammar
  e :: 'e_' ::=
    | x              ::  :: Var
    | ( \ ( x1 .. xn ) e )        ::  :: Lam (+ bind x1 .. xn in e +)
    | call/cc        ::  :: CallCC
    | +              ::  :: Plus
    | A e            ::  :: Abort
    | ( e e1 .. en ) ::  :: App
    | { x1 := e1 ; .. ; xn := en } e'   :: M:: tsub
      {{ ichl ( esubst_e [[x1 e1 .. xn en]] [[e']] ) }}
    | E [ e ]        :: M:: Ctx {{ ichl ( appctx_E_e [[E]] [[e]] ) }}
    | 0 :: :: zero
    | 1 :: :: one
    | 2 :: :: two
    | 3 :: :: three

  v :: 'v_' ::=
    | ( \ ( x1 .. xn ) e )        ::  :: Lam (+ bind x1 .. xn in e +)
    | call/cc        ::  :: CallCC
    | +              ::  :: Plus

  ctx, E :: 'E_' ::=
    | ( v v1 .. vn E1 [ __ ] e1 .. em ) :: :: AppArgCtx
    | ( E1 [ __ ] e1 .. en ) :: :: AppCtx
    | __ :: :: HoleCtx

  terminals :: 'terminals_' ::=
    | \              ::  :: lambda {{ tex \lambda }}
    | +              ::  :: plus
    | call/cc        ::  :: callcc
    | A              ::  :: abort
    | __             ::  :: hole {{ tex \square }}
    | free in        ::  :: free_in
    | ->             ::  :: arrow {{ tex \to }}
    | :=             ::  :: gets {{ tex \gets }}
    | -->            ::  :: reduces {{ tex \longrightarrow }}

  formula :: 'formula_' ::=
    | judgement           ::  :: judgement
    | x free in e            ::  :: xfree
      {{ lem not (List.elem [[x]] (fv_e [[e]])) }}

contextrules
  E _:: e :: e

subrules
  v <:: e

freevars
  e x :: fv

substitutions
  multiple e x :: esubst

defns
  Jop :: '' ::=

defn
  e1 --> e2 :: :: reduce :: '' by

  -------------- :: ax_abort
  E[(A e)] --> e


  x free in E[(call/cc v)]
  ---------------------------------------- :: ax_callcc
  E[(call/cc v)] --> E[(v (\(x) (A E[x])))]

  -------------------------------------------------------------- :: ax_beta
  E[((\ (x1 .. xn) e) v1 .. vn)] --> E[{ x1:=v1 ; .. ; xn:=vn } e]
