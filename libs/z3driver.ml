open Z3

module FuncDeclHashtbl = Hashtbl.Make(struct
    open FuncDecl
    type t = func_decl
    let equal = equal
    let hash (FuncDecl x) =
      AST.hash x
  end)

module ExprHashtbl = Hashtbl.Make(struct
    open Expr
    type t = expr
    let equal = equal
    let hash (Expr x) =
      AST.hash x
  end)

let con_map : string FuncDeclHashtbl.t =
  FuncDeclHashtbl.create 17

type ctx =
  { ctx_z3 : Z3.context
  ; ctx_solver : Solver.solver
  ; ctx_rels : (string * (Expr.expr list ->
                          (Expr.expr * Quantifier.quantifier * Expr.expr list *
                           (Expr.expr list * FuncDecl.func_decl * Expr.expr list) list))) list FuncDeclHashtbl.t
  ; ctx_funs : (string * Expr.expr * Expr.expr list) FuncDeclHashtbl.t

  ; ctx_fifo : Expr.expr Queue.t

  ; ctx_new_guards : (FuncDecl.func_decl * Expr.expr list) ExprHashtbl.t
(*  ; ctx_guards : (FuncDecl.func_decl * Expr.expr list * Expr.expr) ExprHashtbl.t*)
  ; ctx_decls : (Expr.expr list -> Expr.expr) FuncDeclHashtbl.t
  ; mutable ctx_assumptions : (string * (string * Expr.expr) list * Expr.expr) list
  ; ctx_assumed : (string, (string * Expr.expr) list) Hashtbl.t
  ; ctx_expanded : unit ExprHashtbl.t
  ; ctx_guarded : unit ExprHashtbl.t
  ; ctx_trap : Expr.expr
  ; mutable ctx_rinsert : Expr.expr list
  }

let display ({ ctx_z3; _ } as ctx) =
  FuncDeclHashtbl.iter (fun fd fn ->
      let args =
        List.map (Expr.mk_fresh_const ctx_z3 "x") (FuncDecl.get_domain fd) in
      let q =
        Quantifier.mk_forall_const ctx_z3 args
          (Boolean.mk_eq ctx_z3 (Expr.mk_app ctx_z3 fd args) (fn args))
          None [] [] None None in
      Format.eprintf "%s@." @@
      Quantifier.to_string q)
    ctx.ctx_decls;
  Format.eprintf "@.@.%s@." @@ Solver.to_string ctx.ctx_solver

(*let add_guard ctx guard v =
  ExprHashtbl.add ctx.ctx_guards guard v;
  ctx.ctx_rinsert <- guard :: ctx.ctx_rinsert
*)
let mk_ctx z3 =
  { ctx_z3 = z3
  ; ctx_funs = FuncDeclHashtbl.create 17
  ; ctx_new_guards = ExprHashtbl.create 17
  ; ctx_fifo = Queue.create ()
(*  ; ctx_guards = ExprHashtbl.create 17*)
  ; ctx_rinsert = []
  ; ctx_decls = FuncDeclHashtbl.create 17
  ; ctx_rels = FuncDeclHashtbl.create 17
  ; ctx_assumptions = []
  ; ctx_assumed = Hashtbl.create 17
  ; ctx_expanded = ExprHashtbl.create 17
  ; ctx_guarded = ExprHashtbl.create 17
  ; ctx_trap = Expr.mk_fresh_const z3 "trap" (Boolean.mk_sort z3)
  ; ctx_solver = Solver.mk_solver z3 None
  }

let define_fun
    ctx (f : FuncDecl.func_decl) (args : Expr.expr list) (body : Expr.expr)
  : unit =
  Format.eprintf "Define %s = %s@."
    (Expr.to_string @@ FuncDecl.apply f args)
    (Expr.to_string @@ body);
  FuncDeclHashtbl.add ctx.ctx_decls f (Expr.substitute body args);
  FuncDeclHashtbl.add ctx.ctx_funs f ("__fun__", body, args);

type z3_mode = | Mode_exists | Mode_trap | Mode_raw | Mode_restricted
let z3_mode = Mode_raw

let _ =
  Format.eprintf "\x1b[42mUsing mode %s.\x1b[m@." (match z3_mode with
      | Mode_exists -> "exists"
      | Mode_trap -> "trap"
      | Mode_raw -> "raw"
      | Mode_restricted -> "restricted")

(** [compute_deps ctx body] computes the dependencies to inductive
    relations and recursive functions in [body].

    That is, it returns a list of [(conds, fn, args)] where

     - [cond] is a list of expressions that must all be true for the
       dependency to hold

     - [fn] is a function declaration representing an inductive
       relation or recursive function

     - [args] are the arguments to [fn] in the dependency, that will
       have to be passed to the appropriate expansion function for
       [fn].
*)
let rec compute_deps (ctx : ctx) (body : 'a)
  : (Expr.expr list * FuncDecl.func_decl * Expr.expr list) list =
  (* Format.eprintf "Extracting from %s@." (Expr.to_string body); *)
  let { ctx_z3; ctx_solver; _ } = ctx in
  if Expr.is_numeral body then
    []
  else if Boolean.is_ite body then
    let [ if_; then_; else_ ] = Expr.get_args body in
    let if_deps = compute_deps ctx if_ in
    let then_deps = compute_deps ctx then_ in
    let else_deps = compute_deps ctx else_ in
    if_deps @
    List.map (fun (conds, fn, args) -> if_ :: conds, fn, args) then_deps @
    List.map (fun (conds, fn, args) ->
        (Boolean.mk_not ctx_z3 if_) :: conds, fn, args) else_deps
  else
    let args = Expr.get_args body in
    let args_deps =
      List.rev_map (fun a -> (a, compute_deps ctx a)) args in
    let nodeps, deps =
      List.partition (fun (_, conds) -> conds = []) args_deps in
    if Boolean.is_and body then
      (* We only need to consider arguments with dependencies when all
         non-dependent arguments are true *)
      let nodeps_cond = Boolean.mk_and ctx_z3 (List.rev_map fst nodeps) in
      List.flatten @@
      List.rev_map
        (fun (_, allconds) ->
           List.rev_map (fun (conds, fn, args) ->
               (nodeps_cond :: conds, fn, args)) allconds)
        args_deps
    else if Boolean.is_or body then
      (* We only need to consider dependent arguments when all
         non-dependent arguments are false *)
      let nodeps_cond = Boolean.mk_not ctx_z3 @@
        Boolean.mk_or ctx_z3
          (List.rev_map fst nodeps) in
      List.flatten @@
      List.rev_map
        (fun (_, allconds) ->
           List.rev_map (fun (conds, fn, args) ->
               (nodeps_cond :: conds, fn, args)) allconds)
        args_deps
    else
      let fd = Expr.get_func_decl body in
      let args_deps = List.rev_map snd args_deps in
      Format.eprintf "";
      (* TODO: Cleanly separate. *)
      if FuncDeclHashtbl.mem ctx.ctx_rels fd || FuncDeclHashtbl.mem ctx.ctx_funs fd
      then
        ([], fd, args) :: List.flatten args_deps
      else
        List.flatten args_deps

(** [define_rel ctx name r rules] registers a relation [r] as a
    recursive relation, with rules specified by [rules].

    A rule is [name * quant * cond * args] where

    - [name] is the name of the rule

    - [quant] is a list of variables over which the rule is
      existentially quantified

    - [cond] is the hypothesis under which the rule applied (variables
      in [quant] will be quantified)

    - [args] are the actual arguments to the relation (variables in
      [quant] will be quantified)
*)
let define_rel2
  ctx (name : string) (r : FuncDecl.func_decl)
  (rules : (string * Expr.expr list * Expr.expr * Expr.expr list) list)
  : unit =
  let { ctx_z3; ctx_solver; _ } = ctx in
  let rule_expanders =
    List.rev_map (fun (rule_name, quant, cond, args) ->
        (* Compute a function that, given actual arguments to the
           relation, generates all the conditions that must be
           satisfied for the corresponding rule to apply. *)
        (rule_name, function real_args ->
            (* Instantiate the quantifiers with fresh skolemized variables *)
            let fresh =
              List.map (fun q ->
                  Expr.mk_fresh_const ctx_z3 ("sk_" ^ Expr.to_string q) (Expr.get_sort q))
                quant in
            (* Substitute the skolemized variables in the relation's arguments *)
            let sk_args =
              List.map (fun a -> Expr.substitute a quant fresh) args in
            let args_eq = List.map2 (Boolean.mk_eq ctx_z3) real_args sk_args in
            let body =
              Boolean.mk_and ctx_z3 @@
              Expr.substitute cond quant fresh :: args_eq in
            let q =
              Quantifier.mk_exists_const ctx_z3 quant
                (Boolean.mk_and ctx_z3
                   (List.map2 (Boolean.mk_eq ctx_z3) real_args args))
                None [] [] None None in
            (body, q, args_eq, []))) rules
  in
  FuncDeclHashtbl.add ctx.ctx_rels r rule_expanders

type node =
  | App of string * node list
  | Int of Big_int.big_int

let rec pp_list sep pp fmt = function
  | [] -> ()
  | [ x ] -> Format.fprintf fmt "%a" pp x
  | x :: xs -> Format.fprintf fmt "%a%s@,%a" pp x sep (pp_list sep pp) xs

let rec pp_node parens fmt = function
  | Int i -> Format.fprintf fmt "%s" (Big_int.string_of_big_int i)
  | App (head, []) ->
    Format.fprintf fmt "@[%s@]" head
  | App (head, args) ->
    Format.fprintf fmt "@[%s@ %a@]" head (pp_list "" parens) args

let rec y first f fmt x =
  if first then
    Format.fprintf fmt "%a@," (f (y false f)) x
  else
    match x with
    | App (_, []) | Int _ ->
      Format.fprintf fmt "@[%a@]@," (f (y false f)) x
    | _ ->
      Format.fprintf fmt "@[(%a)@]@," (f (y false f)) x

let pp_node = y true pp_node

let rec pp_node fmt = function
  | Int i -> Format.fprintf fmt "%s"
               (Big_int.string_of_big_int i)
  | App ("list_cons", args) | App ("cons", args) ->
    pp_node fmt (App ("List.::", args))
  | App ("List_nil", []) | App ("nil", []) ->
    pp_node fmt (App ("List.nil", []))
  | App (head, []) ->
    Format.fprintf fmt "@[%s@]" head
  | App (head, args) ->
    Format.fprintf fmt "(@[%s@, %a@])" head (pp_list " " pp_node) args

let get_const_id map con =
  match con with
  | "list_cons" -> 0
  | "List_nil" -> 0
  | _ ->
    try
      List.assoc con map
    with Not_found -> Format.eprintf "Oops %s@." con; failwith "noo!!"

let rec native_of_node map = function
  | Int i -> Obj.repr (Big_int.int_of_big_int i)
  | App (head, []) ->
    Obj.repr (get_const_id map head)
  | App (head, args) ->
    let nb_args = List.length args in
    let native_args = List.mapi (fun i arg ->
        native_of_node map arg) args in
    let repr = Obj.new_block (get_const_id map head) nb_args in
    List.iteri (fun i arg ->
        Obj.set_field repr i arg) native_args;
    repr

let rec node_of_expr e =
  if Arithmetic.is_int e then
    Int (Arithmetic.Integer.get_big_int e)
  else
    let fd = Expr.get_func_decl e in
    let head =
      try
        FuncDeclHashtbl.find con_map fd
      with Not_found ->
        Symbol.to_string @@ FuncDecl.get_name @@ fd in
    let args = List.map node_of_expr @@ Expr.get_args e in
    App (head, args)

let pp_expr fmt e = pp_node fmt (node_of_expr e)

let expr_to_string e =
  Format.flush_str_formatter () |> ignore;
  Format.fprintf Format.str_formatter "%a@." pp_expr e;
  Format.flush_str_formatter ()

(** [extract_deps ctx body] extract the recursive dependencies from
    [body] into [ctx] and returns a new expression that should be used
    instead of [body] in all other expressions.

    More specifically, fresh variables are created for the various
    guards in [body], and added to the [ctx_guards] of [ctx], and a
    fresh variable [v] is created, and expressions are added to [ctx]
    so that [v = body] holds.

    Note that [body] need not be a boolean expression, and even if it
    is, [body] is not asserted into the solver.
*)
    (*
let rec extract_deps ctx body =
  (* Format.eprintf "Extracting from %s@." (Expr.to_string body); *)
  let { ctx_z3; ctx_decls; ctx_solver; _ } = ctx in
  let rec extract_deps conds res body =
    if Expr.is_numeral body then
      [], [ Expr.simplify
              (Boolean.mk_implies ctx_z3 (Boolean.mk_and ctx_z3 conds)
                 (Boolean.mk_eq ctx_z3 res body)) None ]
    else if Boolean.is_ite body then
      let [ if_; then_; else_ ] = Expr.get_args body in
      let b = Expr.mk_fresh_const ctx_z3 "b" (Boolean.mk_sort ctx_z3) in
      (* [if_eqn] expresses that [conds => b = if_] with guards [if_guards] *)
      let if_guards, if_eqn = extract_deps conds b if_ in
      (* [then_eqn] expresses that [conds /\ b => res = then_] with guards [then_guards] *)
      let then_guards, then_eqn = extract_deps (b :: conds) res then_ in
      (* [else_eqn] expresses that [conds /\ ~b => res = else_] with guards [else_guards] *)
      let else_guards, else_eqn =
        extract_deps (Boolean.mk_not ctx_z3 b :: conds) res else_ in
      (* We want
         [(conds => b = if_) /\ (conds /\ b => res = then_) /\ (conds /\ ~b => res = else_)]
         with guards [if_guards ; then_guards; else_guards] *)
      List.rev_append if_guards (List.rev_append then_guards else_guards),
      List.rev_append if_eqn (List.rev_append then_eqn else_eqn)
      (* [DONE]: Same things with [or] - in [A \/ B \/ C] if [A] is nonrec
         and true, no need to expand [B] and [C] so we can add [~A] into
         all the guards. *)
      (* Here we say in [A /\ B /\ C], if [A] is nonrec and false,
         no need to expand [B] and [C] so we can add [A] into all the
         guards. *)
    else if Boolean.is_and body then
      let args = Expr.get_args body in
      (* Perform a recursive call on the arguments *)
      let rec_calls =
        List.rev_map (fun e ->
            let res = Expr.mk_fresh_const ctx_z3 "tmp" (Expr.get_sort e) in
            let guards, eqn = extract_deps conds res e in
            (res, (guards, eqn))) args in
      (* [rec_calls] is [ [(b, (guards, {{ b = ith_arg }}))] ] *)
      (* We want to compute the unguarded arguments first *)
      let unguarded_calls, guarded_calls =
        List.partition (fun (_, (guards, _)) -> guards = []) rec_calls in
      (* First we compute the AND of all unguarded calls. *)
      let b = Expr.mk_fresh_const ctx_z3 "unguarded_and" (Boolean.mk_sort ctx_z3) in
      let b_eq_unguarded = Boolean.mk_eq ctx_z3 b @@
        Expr.simplify
          (Boolean.mk_and ctx_z3 (List.rev_map fst unguarded_calls))
          None in
      (* Then we add  this AND into all the guards -
         indeed if any of the unguarded arguments is false, expanding the
         guarded ones will never make the whole AND expression true. *)
      let guarded_calls =
        List.rev_map (fun (var, (guards, eqn)) ->
            (var, (List.rev_map (fun (guard, rec_call) ->
                 (Expr.simplify (Boolean.mk_and ctx_z3 @@ [b ; guard]) None, rec_call)) guards,
                   eqn))) guarded_calls in
      (*  Now we can return the definitions & extended guards *)
      let all_guards, all_guarded_eqns =
        List.split (List.rev_map snd guarded_calls) in
      let _, all_unguarded_eqns =
        List.split (List.rev_map snd unguarded_calls) in
      let body' = Boolean.mk_and ctx_z3 @@
        b :: (List.rev_map fst guarded_calls) in
      List.concat all_guards,
      (Boolean.mk_implies ctx_z3 (Boolean.mk_and ctx_z3 conds)
         (Boolean.mk_eq ctx_z3 body' res)) :: b_eq_unguarded ::
      List.rev_append (List.concat all_guarded_eqns)
        (List.concat all_unguarded_eqns)
    else if Boolean.is_or body then
      let args = Expr.get_args body in
      (* Perform a recursive call on the arguments *)
      let rec_calls =
        List.rev_map (fun e ->
            let res = Expr.mk_fresh_const ctx_z3 "tmp" (Expr.get_sort e) in
            let guards, eqn = extract_deps conds res e in
            (res, (guards, eqn))) args in
      (* [rec_calls] is [ [(b, (guards, {{ b = ith_arg }}))] ] *)
      (* We want to compute the unguarded arguments first *)
      let unguarded_calls, guarded_calls =
        List.partition (fun (_, (guards, _)) -> guards = []) rec_calls in
      (* First we compute the OR of all unguarded calls. *)
      let b = Expr.mk_fresh_const ctx_z3 "unguarded_or" (Boolean.mk_sort ctx_z3) in
      let b_eq_unguarded = Boolean.mk_eq ctx_z3 b @@
        Expr.simplify
          (Boolean.mk_or ctx_z3 (List.rev_map fst unguarded_calls))
          None in
      (* Then we add the negation of this OR into all the guards -
         indeed if any of the unguarded arguments is true, expanding the
         guarded ones will never make the whole OR expression false. *)
      let guarded_calls =
        List.rev_map (fun (var, (guards, eqn)) ->
            (var, (List.rev_map (fun (guard, rec_call) ->
                 (Expr.simplify (Boolean.mk_and ctx_z3 @@
                                 [Boolean.mk_not ctx_z3 b ; guard]) None, rec_call)) guards,
                   eqn))) guarded_calls in
      (*  Now we can return the definitions & extended guards *)
      let all_guards, all_guarded_eqns =
        List.split (List.rev_map snd guarded_calls) in
      let _, all_unguarded_eqns =
        List.split (List.rev_map snd unguarded_calls) in
      let body' = Boolean.mk_or ctx_z3 @@
        b :: (List.rev_map fst guarded_calls) in
      List.concat all_guards,
      (Boolean.mk_implies ctx_z3 (Boolean.mk_and ctx_z3 conds)
         (Boolean.mk_eq ctx_z3 body' res)) :: b_eq_unguarded ::
      List.rev_append (List.concat all_guarded_eqns)
        (List.concat all_unguarded_eqns)
    else
      let fd = Expr.get_func_decl body in
      let args = Expr.get_args body in
      let rargs, guards, eqn =
        List.fold_left (fun (rargs, guards, eqn) e ->
            let r = Expr.mk_fresh_const ctx_z3 "rule" (Expr.get_sort e) in
            (* Format.eprintf "%a = %a@." pp_expr r pp_expr e; *)
            let (guards', eqn') = extract_deps conds r e in
            let r, eqn' =
              (r, eqn') in
            (r :: rargs, List.rev_append guards' guards, List.rev_append eqn' eqn))
          ([], [], []) args in
      let body' = FuncDecl.apply fd (List.rev rargs) in
      let guard = match conds with
        | [] -> Boolean.mk_true ctx_z3
        | _ -> Expr.simplify (Boolean.mk_and ctx_z3 conds) None in
      let guards =
        let fn = body in
        if ExprHashtbl.mem ctx.ctx_expanded fn then begin
          Format.eprintf "Already expanded %s@." @@ Expr.to_string body;
          guards
        end else
          try
            (guard, (fd, args, FuncDeclHashtbl.find ctx_decls fd @@ List.rev rargs))
            :: guards
          with Not_found ->
            guards in
      (guards, (Expr.simplify
                  (Boolean.mk_implies ctx_z3 guard (Boolean.mk_eq ctx_z3 res body'))
                  None) :: eqn) in
  let res = Expr.mk_fresh_const ctx_z3 "res" (Expr.get_sort body) in
  let guards, eqn = extract_deps [] res body in
  let guards, eqn =
    List.fold_left (fun (guards, eqn) (guard, rec_call) ->
        let b = Expr.mk_fresh_const ctx_z3 "guard" (Expr.get_sort guard) in
        (b, rec_call) :: guards, Boolean.mk_iff ctx_z3 (Boolean.mk_not ctx_z3 b) guard :: eqn)
      ([], eqn) guards in
  List.iter (fun (guard, rec_call) ->
      add_guard ctx guard rec_call) guards;
  Solver.add ctx_solver eqn;
  res
*)
(** TODO: Document *)
let guard_deps ctx guard deps =
  List.iter (fun (cond, fd, args) ->
      if false && (* BUGUPDATE *) ExprHashtbl.mem ctx.ctx_guarded (FuncDecl.apply fd args)
      then () else begin
        let guard =
          Expr.mk_fresh_const ctx.ctx_z3 ("guard_deps")
            (Boolean.mk_sort ctx.ctx_z3) in
        Format.eprintf "Guarding %s \x1b[046mwith\x1b[m %s=%s@."
          (Expr.to_string @@ FuncDecl.apply fd args)
          (Expr.to_string guard)
          (Expr.to_string @@ Boolean.mk_and ctx.ctx_z3 cond);
        let res =
          if cond = [] then Boolean.mk_true ctx.ctx_z3
          else Boolean.mk_and ctx.ctx_z3 cond in
        Solver.add ctx.ctx_solver
          [ Boolean.mk_implies ctx.ctx_z3 res guard ];
        (* And register. *)
        Queue.push (Boolean.mk_not ctx.ctx_z3 guard) ctx.ctx_fifo;
        ExprHashtbl.add ctx.ctx_new_guards (Boolean.mk_not ctx.ctx_z3 guard)
          (fd, args);
        ExprHashtbl.add ctx.ctx_guarded (FuncDecl.apply fd args) ()
      end) deps
  (*Solver.add ctx.ctx_solver [
    Boolean.mk_implies ctx.ctx_z3 guard @@
    Boolean.mk_or ctx.ctx_z3 @@ List.flatten @@ List.map (fun (cond, _, _) -> cond) deps
  ]*)

(** [expand_rel ctx fd args] expands the call to relation [fd] with
    arguments [args] according to the information stored in [ctx]'s
    [ctx_rels] field, and adds the needed guards around recursive
    calls.

    Note that [fd] must be a registered relation in [ctx]'s [ctx_rel]
    field. *)
let expand_rel (ctx : ctx) (top : Expr.expr) (fd : FuncDecl.func_decl) (args : Expr.expr list) =
  Format.eprintf "Expanding %s@." @@ Expr.to_string @@ FuncDecl.apply fd args;
  let rules = FuncDeclHashtbl.find ctx.ctx_rels fd in
  let nrules = List.length rules in
  let picker = Expr.mk_fresh_const ctx.ctx_z3 "picker"
      (Arithmetic.Integer.mk_sort ctx.ctx_z3) in
  (*
  Format.eprintf "EXPAND REL %s (%d) with picker %s@."
    (Expr.to_string @@ Expr.mk_app ctx.ctx_z3 fd args)
    nrules
    (Expr.to_string picker); *)
  (*  List.iteri (fun i (name, _) -> Format.eprintf "%d=%s ; " (i+1) name) rules;
      Format.eprintf "@.";*)
  let all_hyps =
    List.mapi (fun i (name, builder) ->
        let expanded_rule, quant, args_eq, _real_deps = builder args in
        let real_deps = compute_deps ctx expanded_rule in
        let deps =
          List.rev_map (fun (cond, fd, args) ->
              (Boolean.mk_eq ctx.ctx_z3 picker
                 (Arithmetic.Integer.mk_numeral_i ctx.ctx_z3 (i+1)))
              :: cond, fd, args) real_deps in
        Solver.add ctx.ctx_solver [
          Boolean.mk_implies ctx.ctx_z3 (* BUGUPDATE: iff -> implies *)
            (Boolean.mk_eq ctx.ctx_z3 picker
               (Arithmetic.Integer.mk_numeral_i ctx.ctx_z3 (i+1)))
            expanded_rule
        ];
        let guard =
          Expr.mk_fresh_const ctx.ctx_z3 ("guard_" ^ name)
            (Boolean.mk_sort ctx.ctx_z3) in
        let deps =
          List.map (fun (cond, fd, args) -> (top :: cond, fd, args)) deps in
        guard_deps ctx guard deps;
        (* Format.eprintf "Args eq? %s@." @@ Expr.to_string @@ Boolean.mk_and ctx.ctx_z3 args_eq;*)
        (* Existential skolemisation check (!! BROKEN !!) *)
        if z3_mode = Mode_exists then begin
          Format.eprintf "\x1b[037mAdding %s\x1b[m@." @@ Expr.to_string @@
          Boolean.mk_implies ctx.ctx_z3
            (Boolean.mk_and ctx.ctx_z3 [
                (Boolean.mk_eq ctx.ctx_z3 picker
                   (Arithmetic.Integer.mk_numeral_i ctx.ctx_z3 0)) ;
                Quantifier.expr_of_quantifier quant
              ])
            (Boolean.mk_and ctx.ctx_z3 args_eq);
          Solver.add ctx.ctx_solver [
            Boolean.mk_implies ctx.ctx_z3
              (Boolean.mk_and ctx.ctx_z3 [
                  (Boolean.mk_eq ctx.ctx_z3 picker
                     (Arithmetic.Integer.mk_numeral_i ctx.ctx_z3 0)) ;
                  Quantifier.expr_of_quantifier quant
                ])
              (Boolean.mk_and ctx.ctx_z3 args_eq)
          ]
        end; expanded_rule) rules in
  (*
  Solver.add ctx.ctx_solver [
    Boolean.mk_implies ctx.ctx_z3
      (Boolean.mk_eq ctx.ctx_z3 picker
         (Arithmetic.Integer.mk_numeral_i ctx.ctx_z3 0))
      (Boolean.mk_not ctx.ctx_z3 (Boolean.mk_or ctx.ctx_z3 all_hyps))
  ];*) (* BUGUP added *)
  Solver.add ctx.ctx_solver [ Boolean.mk_and ctx.ctx_z3 [
      Arithmetic.mk_le ctx.ctx_z3 picker @@
      Arithmetic.Integer.mk_numeral_i ctx.ctx_z3 nrules ;
      Arithmetic.mk_ge ctx.ctx_z3 picker @@
      Arithmetic.Integer.mk_numeral_i ctx.ctx_z3 0 ] ];
  Solver.add ctx.ctx_solver [
    Boolean.mk_iff ctx.ctx_z3 (Expr.mk_app ctx.ctx_z3 fd args)
      (Boolean.mk_distinct ctx.ctx_z3
         [ picker; Arithmetic.Integer.mk_numeral_i ctx.ctx_z3 0 ])
  ]

(*  let all_deps =
    List.flatten deps_and_rules in @@ (List.map snd deps_and_rules) in
  let expanded_rules = List.map fst deps_and_rules in*)

  (*
  let all_deps =
    List.map (fun (cond, fd, args) ->
        (Boolean.mk_eq ctx.ctx_z3 picker (Arithmetic.Integer.mk_numeral_i ctx.ctx_z3 0)::cond,fd,args))
      all_deps in *)
  (*
  List.iter (fun (cond, fd, args)->
      Format.eprintf "%s for %s@.@." (Expr.to_string @@ Boolean.mk_and ctx.ctx_z3 cond) (Expr.to_string @@ FuncDecl.apply fd args)) all_deps;
  let guard =
    Expr.mk_fresh_const ctx.ctx_z3 ("picker_guard_")
      (Boolean.mk_sort ctx.ctx_z3) in
  guard_deps ctx guard all_deps; *)
(*  Solver.add ctx.ctx_solver [
    Boolean.mk_implies ctx.ctx_z3
      (Boolean.mk_eq ctx.ctx_z3 picker (Arithmetic.Integer.mk_numeral_i ctx.ctx_z3 0))
      guard
  ];*)

let expand_fun (ctx : ctx) (top : Expr.expr) (fd : FuncDecl.func_decl) (args : Expr.expr list) =
  Format.eprintf "EXPAND FUN %s: %s@." (FuncDecl.to_string fd) @@ Expr.to_string @@ FuncDecl.apply fd args;
  let name, body, def_args = FuncDeclHashtbl.find ctx.ctx_funs fd in
  let expanded_body = Expr.substitute body def_args args in
  let deps = compute_deps ctx body in
  let deps =
    List.map
      (fun (conds, fd, dep_args) ->
         (List.map (fun e -> Expr.substitute e def_args args) conds, fd,
          List.map (fun e -> Expr.substitute e def_args args) dep_args))
      deps in
  let guard =
    Expr.mk_fresh_const ctx.ctx_z3 ("guard_" ^ name)
      (Boolean.mk_sort ctx.ctx_z3) in
  let deps = List.map (fun (cond, fd, args) -> (top :: cond, fd, args)) deps in
  guard_deps ctx guard deps;
  Solver.add ctx.ctx_solver
    [ Boolean.mk_eq ctx.ctx_z3
        (Expr.mk_app ctx.ctx_z3 fd args) expanded_body ]

let assume ctx name skolems expr =
  ctx.ctx_assumptions <- (name, skolems, expr) :: ctx.ctx_assumptions

let expanded = FuncDeclHashtbl.create 17
let nb = ref 0

let expand_guard ctx e =
  (* Format.eprintf "Expanding %s@." @@ Expr.to_string e;*)
  (* Expand new-style guards *)
  let nguarded = ExprHashtbl.find_all ctx.ctx_new_guards e in
  List.iter (fun _ ->
      ExprHashtbl.remove ctx.ctx_new_guards e) nguarded;
  List.iter (fun (fd, args) ->
      try
        raise Not_found
(*  TODO: Inductive/CoInductive       ExprHashtbl.find ctx.ctx_expanded (FuncDecl.apply fd args) |> ignore*)
      with Not_found ->
        FuncDeclHashtbl.add expanded fd ();
        ExprHashtbl.add ctx.ctx_expanded (FuncDecl.apply fd args) ();
        if FuncDeclHashtbl.mem ctx.ctx_rels fd then
          expand_rel ctx (Boolean.mk_not ctx.ctx_z3 e) fd args
        else
          expand_fun ctx (Boolean.mk_not ctx.ctx_z3 e) fd args)
    nguarded(*;

  (* Expand old-style guards *)
  let guarded = ExprHashtbl.find_all ctx.ctx_guards e in
  ExprHashtbl.remove ctx.ctx_guards e;
  List.iter (fun (fd, args, body) ->
      Hashtbl.add expanded fd ();
      Format.eprintf "Expanding %s@." (Expr.to_string @@
                                      FuncDecl.apply fd args))
    guarded;
  List.iter (fun (fd, args, body) ->
      try
        ExprHashtbl.find ctx.ctx_expanded (FuncDecl.apply fd args) |> ignore
      with Not_found ->
        ExprHashtbl.add ctx.ctx_expanded (FuncDecl.apply fd args) ();
        Solver.add ctx.ctx_solver [
          Boolean.mk_eq ctx.ctx_z3 (FuncDecl.apply fd args) (extract_deps ctx body)
        ])
              guarded *)

let out = Hashtbl.create 17

let rec check_n con_map n ctx =
  nb := !nb + 1;
  List.iter (fun (n, sks, e) ->
(*      Solver.add ctx.ctx_solver [ extract_deps ctx e ];*)
      (** NEW *)
      let deps = compute_deps ctx e in
      let guard =
        Expr.mk_fresh_const ctx.ctx_z3 "initial_guard"
          (Boolean.mk_sort ctx.ctx_z3) in
      guard_deps ctx guard deps;
      Solver.add ctx.ctx_solver [ e ];
      (** END NEW *)
      Hashtbl.add ctx.ctx_assumed n sks)
    ctx.ctx_assumptions;
  ctx.ctx_assumptions <- [];
  let all_guards =
(*    ExprHashtbl.fold (fun guard _ all_guards -> guard :: all_guards)
      ctx.ctx_guards @@*)
    ExprHashtbl.fold (fun guard (fd, args) all_guards ->
        (*Format.eprintf "%s guarding %s@." (Expr.to_string guard) @@ Expr.to_string @@ Expr.mk_app ctx_z3 fd args;*)
        guard :: all_guards)
      ctx.ctx_new_guards []
  in
  Format.eprintf "\x1b[45mSolving %d.\x1b[m@." (List.length @@ Solver.get_assertions ctx.ctx_solver)(*" with %s...@."
    (ExprHashtbl.fold (fun g (fd, es, _) s ->
         Expr.to_string g ^ " => " ^ Expr.to_string (FuncDecl.apply fd es) ^ " ; " ^ s) ctx.ctx_guards "")*);
  Format.eprintf "K.@.";
  (* TODO: Luckiness *)
  match Solver.check ctx.ctx_solver all_guards with
  | Solver.SATISFIABLE ->
    Format.eprintf "DONE@.";
    begin match Solver.get_model ctx.ctx_solver with
      | None -> failwith "<Model unavailable>"
      | Some model ->
        (* Format.eprintf "Checking for trap...@." ; *)
        (* begin match nModel.get_const_interp_e model ctx.ctx_trap with *)
        (*   | None -> "<no trap>" *)
        (*   | Some t -> Expr.to_string t *)
        (* end; *)
        if false && Solver.check ctx.ctx_solver (Boolean.mk_not ctx.ctx_z3 ctx.ctx_trap :: all_guards)
           != Solver.SATISFIABLE
        then begin
          Format.eprintf "\x1b[41mNEED TO PROVE FALSITY. UNSUPPORTED YET.\x1b[m@.";
          Solver.UNKNOWN
        end else begin
(*          Format.eprintf "FULL MODEL %s@." @@ Model.to_string model;*)
          Format.eprintf "DUMP: %s@." @@ Solver.to_string ctx.ctx_solver;
          Hashtbl.iter (fun n bnds ->
              Format.eprintf "Values bound by %s:@." n;
              List.iter (fun (skn, ske) ->
                  let e =
                    match Model.get_const_interp_e model ske with
                    | None -> failwith "Wrong."
                    | Some e -> e in
                  let native_e = native_of_node con_map (node_of_expr e) in
                  Hashtbl.add out skn native_e;
                  Format.eprintf "  %s = %a@." skn pp_expr e)
                bnds)
            ctx.ctx_assumed;
          Solver.SATISFIABLE
        end
    end
  | Solver.UNKNOWN -> Solver.UNKNOWN
  | Solver.UNSATISFIABLE ->
    let unsat_core =
      List.rev_map Expr.expr_of_ast @@ Solver.get_unsat_core ctx.ctx_solver in
    Format.eprintf "Core is %s...@."
      (List.fold_left (fun acc e ->
           Expr.to_string e ^ " /\\ " ^ acc) "" unsat_core);
    if unsat_core = [] then begin
      Format.eprintf "DUMP: %s@." @@ Solver.to_string ctx.ctx_solver;
      Solver.UNSATISFIABLE
    end
    else begin
      let nb = ref 0 in
      let maxi = min
          (List.length ctx.ctx_rinsert / 2 - List.length unsat_core)
          0
      in
      Format.eprintf "%d@." (List.length ctx.ctx_rinsert);
      begin try
          while !nb < maxi && ctx.ctx_rinsert != [] do
            match List.rev ctx.ctx_rinsert with
            | [] -> ()
            | g :: rst ->
              ctx.ctx_rinsert <- List.rev rst;
              begin try
                  ExprHashtbl.find ctx.ctx_new_guards g |> ignore;
                  expand_guard ctx g;
                  nb := !nb + 1
                with Not_found -> ()
              end
          done
        with Not_found -> ()
      end;
      let take_n = 200 in
      let what_to_expand = (*begin
        let res = ref [] in
        begin try
            for i = 1 to take_n do
              res := Queue.pop ctx.ctx_fifo :: !res
            done
          with Queue.Empty -> () end;
        !res
      end*)
        unsat_core(*all_guards*)
      in
      begin
        Format.eprintf "\x1b[035mExpanding %d/%d/%d@.\x1b[m" (List.length what_to_expand)
          (List.length unsat_core) (List.length all_guards);
        List.iter (expand_guard ctx) what_to_expand
      end;
      if n = 0 then begin
        (* ExprHashtbl.clear ctx.ctx_new_guards; check_n (-1) ctx |> ignore; *)
        (* Format.eprintf "DUMP: %s@." @@ Solver.to_string ctx.ctx_solver; *)
        Solver.UNKNOWN end else
        check_n con_map (n-1) ctx
    end

let check ctx con_map =
  FuncDeclHashtbl.clear expanded;
  let res = check_n con_map 300 ctx in
  let stats = Solver.get_statistics ctx.ctx_solver in
  Format.eprintf "STATS: %s@." (Solver.Statistics.to_string stats);
  Format.eprintf "Expanded (%d rounds, %d assertions): @." !nb
    (Solver.get_num_assertions ctx.ctx_solver);
  let counters = Hashtbl.create 20 in
  FuncDeclHashtbl.iter (fun fd _ ->
      let name = Symbol.to_string @@ FuncDecl.get_name fd in
      try
        Hashtbl.replace counters name (Hashtbl.find counters name + 1)
      with Not_found ->
        Hashtbl.add counters name 1)
    expanded;
  Hashtbl.iter (Format.eprintf "%s: %d@.") counters;
  (res, Hashtbl.fold (fun k v l -> (k, v) :: l) out [])
