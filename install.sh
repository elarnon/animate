#!/bin/sh

git clone https://bitbucket.org/elarnon/lem.git --bare
git clone lem.git lem_patterns
cd lem_patterns
git checkout pattern-matching
make
make ocaml-libs
cd ..
git clone lem.git lem_z3
cd lem_z3
git checkout z3new
make bin/lem
